import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import TahlilJson from '../.../../../../Tahlil.json';

const Tahlil = () => {
  console.log(TahlilJson);
  const data = TahlilJson;
  return (
    <ScrollView style={StyleS.wadah}>
      {data.map(item => {
        return (
          <View key={item.number}>
            <Text style={StyleS.text1}>{item.text}</Text>
            <Text style={StyleS.text2}>{item.translation_id}</Text>
          </View>
        );
      })}
    </ScrollView>
  );
};

export default Tahlil;
const StyleS = StyleSheet.create({
  wadah: {
    backgroundColor: '#DF711B',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  text1: {
    color: 'white',
    fontSize: 20,
    margin: 10,
  },
  text2: {
    color: 'white',
    fontSize: 16,
    margin: 10,
  },
});
