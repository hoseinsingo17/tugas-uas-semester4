import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';

const Home = ({navigation}) => {
  return (
    <View style={{flex: 1,alignItems:"center",backgroundColor:'#ffffff'}}>
      <Image source={require('../../img/masjid.jpg')} style={{height:230,alignItems:'center',width:500,marginBottom:50}}/>
      <View style={{borderColor:'#DF711B',borderWidth:2,height:200,width:300,alignItems:"center",justifyContent:'center',borderRadius:30}}>
      <TouchableOpacity onPress={() => navigation.navigate('Tahlil')} style={{backgroundColor:'white',height:100,width:100,alignItems:'center',justifyContent:'center',borderRadius:10,shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 3,
            },
            shadowOpacity: 0.27,
            shadowRadius: 4.65,

            elevation: 10,
            borderWidth:2,
            borderColor:'#DF711B'
            }}>
              <Image source={require('../../img/buku.png')} style={{height:35,width:62}}/>
        <Text style={{fontSize:16,marginTop:8,fontWeight:'bold',color:'#DF711B'}}>TAHLIL</Text>
      </TouchableOpacity>
      </View>
    </View>
  );
};

export default Home;
